"use strict";

console.log('arr' + " - object");
console.log(4 + +"5");

let incr = 10,
    decr = 10;

/* incr++;
decr--; */

console.log(incr++);
console.log(decr--);


console.log(5%2); //выводит остаток после деления на 2 (в данном случае 1)

console.log(2*4 === '8'); //false строгое равенство, число не равно строке
console.log(2 + 2 * 2 == 8); //false сначала идет умножение, затем сложение
console.log(2 + 2 * 2 !== 8); //true знак инверсии, выражение не равно 8

const isChecked = true;
    isClose = false;

console.log(isChecked && isClose); //false выполняется, если оба условия true
console.log(isChecked || isClose); //true если есть хотя бы одна правда
console.log(isChecked || !isClose); //true оператор отрицания, меняет false на true

let a = 5,
    b = 4;
console.log(a > b); //true, число 5 больше 4

let result = 5 > 4; //true результат сравнения присваивается переменной result
alert( result );

let resultA = ("ананас" > "яблоко"); //false, порядковый номер Я больше порядкового номера А
alert( resultA );

let resultB = ("2" > "12"); //true, строчное сравнение, 2 в первой строке > 1 во второй строке
alert( resultB );

let resultC = (undefined == null); //true, равны только при нестрогом равенстве

let resultD = (undefined === null); //false, строгое неравенство, различны типы

let resultE = (null == "\n0\n"); //false, при нестрогом равенстве null=undefined и только ему.

let resultF = (null === +"\n0\n"); //false, сравнение разных типов

let resultG = ("" + 1 + 0); // 10, т.к. "" преобразуют число 1 в строку, далее "1" при сложении с 0 также преобразуется в строку

let resultH = ("" - 1 + 0); //-1, ""=0, остается числовое выражение -1+0

let resultI = (true + false); //1, 1+0

let resultK = (6 / "3"); //2, строка числовая и преобразовывается в число

let resultL = ("2" * "3"); //6, числовые строки преобразовываются в числа и умножаются

let resultM = (4 + 5 + "px"); //"9px", числовые складываются, строка прибавляется, получается строка 

let resultN = ("$" + 4 + 5); //"$45", строка превращает числа в строки и прибавляет рядом

let resultO = ("4" - 2); //2, строка числовая превращается в число, далее вычитание между числами

let resultP = ("4px" - 2); //NaN, из строки нельзя вычесть число

let resultQ = (7 / 0); //Infinity, математическая бесконечность при делении на 0

let resultR = ("  -9  " + 5); // "-95" сложение со строкой превращает в строку и добавляет рядом

let resultS = ("  -9  " - 5); //-14 строка числовая превращается в число и вычитается

let resultT = (null + 1); //1, null=0 после численного преобразования

let resultU = (undefined + 1); //NaN, undefined=NaN после численного преобразования

let resultV = (" \t \n" - 2); //-2, пробелы по бокам в строке игнорируются,она приравнивается к пустой и превращается в 0