const firstName = 'Artem';
const lastName = '';
const middleName = ' ';
const isAdmin = false;
const cash = null;
const dateBirthday = undefined;
const login = 'artem@mail.ru';
const age = 25;


//Строковое преобразование
const resultStringFirstName = String(firstName);
const resultStringlastName = String(lastName);
const resultStringmiddleName = String(middleName);
const resultStringIsAdmin = String(isAdmin);
const resultStringCash = String(cash);
const resultStringDateBirthday = String(dateBirthday);
const resultStringAge = String(age);
const resultStringAgeB = age + '';

console.log({resultStringFirstName, resultStringlastName, resultStringmiddleName, resultStringIsAdmin, resultStringCash, resultStringDateBirthday, resultStringAge, resultStringAgeB})

//Численное преобразование
const resultNumberFirstName = Number(firstName);
const resultNumberlastName = Number(lastName);
const resultNumbermiddleName = Number(middleName);
const resultNumberIsAdmin = Number(isAdmin);
const resultNumberCash = Number(cash);
const resultNumberDateBirthday = Number(dateBirthday);
const resultNumberAge = Number(age);
const resultNumberFirstNameUn = +firstName;

console.log({resultNumberFirstName, resultNumberlastName, resultNumbermiddleName, resultNumberIsAdmin, resultNumberCash, resultNumberDateBirthday, resultNumberAge, resultNumberFirstNameUn})

//Логическое преобразование
const resultBooleanFirstName = Boolean(firstName);
const resultBooleanlastName = Boolean(lastName);
const resultBooleanmiddleName = Boolean(middleName);
const resultBooleanIsAdmin = Boolean(isAdmin);
const resultBooleanCash = Boolean(cash);
const resultBooleanDateBirthday = Boolean(dateBirthday);
const resultBooleanAge = Boolean(age);
console.log({resultBooleanFirstName, resultBooleanlastName, resultBooleanmiddleName, resultBooleanIsAdmin, resultBooleanCash, resultBooleanDateBirthday, resultBooleanAge})

//Неявное преобразование
const resultIsAdminAndCash = '0' === 0;

console.log({ resultIsAdminAndCash})